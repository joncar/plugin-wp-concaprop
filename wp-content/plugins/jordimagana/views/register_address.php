<?php 
	$country_obj   = new WC_Countries();
    $countries     = $country_obj->countries;
    $states        = $country_obj->states;
?>
<p class="form-row form-group form-row-wide">
    <label for="shop-phone">
    	<?php esc_html_e( 'Street ', 'dokan-lite' ); ?>
    	<span class="required">*</span>
	</label>
    <input id="dokan_address[street_1]" value="" name="dokan_address[street_1]" placeholder="<?php esc_attr_e( 'Street address' , 'dokan-lite' ) ?>" class="dokan-form-control input-md" type="text">
</p>

<p class="form-row form-group form-row-wide">
    <label for="shop-phone">
    	<?php esc_html_e( 'Street 2', 'dokan-lite' ); ?>    	
	</label>
    <input id="dokan_address[street_2]" value="" name="dokan_address[street_2]" placeholder="<?php esc_attr_e( 'Apartment, suite, unit etc. (optional)' , 'dokan-lite' ) ?>" class="dokan-form-control input-md" type="text">
</p>

<p class="form-row form-group form-row-wide">
    <label for="shop-phone">
    	<?php esc_html_e( 'City', 'dokan-lite' ); ?>
    	<span class="required">*</span>
	</label>
    <input id="dokan_address[city]" value="" name="dokan_address[city]" placeholder="<?php esc_attr_e( 'Town / City' , 'dokan-lite' ) ?>" class="dokan-form-control input-md" type="text">
</p>

<p class="form-row form-group form-row-wide">
    <label for="shop-phone">
    	<?php esc_html_e( 'Post/ZIP Code', 'dokan-lite' ); ?>
    	<span class="required">*</span>
	</label>
    <input id="dokan_address[zip]" value="" name="dokan_address[zip]" placeholder="<?php esc_attr_e( 'Postcode / Zip' , 'dokan-lite' ) ?>" class="dokan-form-control input-md" type="text">
</p>

<p class="form-row form-group form-row-wide">
    <label for="shop-phone">
    	<?php esc_html_e( 'Country ', 'dokan-lite' ); ?>
    	<span class="required">*</span>
	</label>
    <select name="dokan_address[country]" class="country_to_state dokan-form-control" id="dokan_address_country">
        <?php dokan_country_dropdown( $countries,'ES', false ); ?>
    </select>
</p>

<p class="form-row form-group form-row-wide">
    <label for="shop-phone">
    	<?php esc_html_e( 'State ', 'dokan-lite' ); ?>
    	<span class="required">*</span>
	</label>
    <select name="dokan_address[state]" class="dokan-form-control" id="dokan_address_state">
        <?php dokan_state_dropdown( $states['ES'], 'B') ?>
    </select>
</p>