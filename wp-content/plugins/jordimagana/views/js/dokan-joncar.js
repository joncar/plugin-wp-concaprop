(function($){	

  var Dokan_Editor = {
  	init:function(){

  		$('body, .product-edit-container').on('click','.dokan-feat-joncar-image-btn',this.imageUpload);
      $('body').on('click','a.add-product-joncar-images',this.imageGalleryUpload);      
  	},
    imageGalleryUpload:function(e){
      e.preventDefault();      
      Dokan_Editor.prepareGallery();
      Dokan_Editor.doProcessImage = Dokan_Editor.setImageGallery;
      Dokan_Editor.frame.open();
      Dokan_Editor.self = $(this);
    },
  	imageUpload:function(e){  		
  		e.preventDefault();      
      Dokan_Editor.prepareGallery();
      Dokan_Editor.doProcessImage = Dokan_Editor.setImage;
	    Dokan_Editor.frame.open();
	    Dokan_Editor.self = $(this);
  	},
    prepareGallery:function(){      
      var settings = Dokan_Editor;
      Dokan_Editor.frame = wp.media({
          // Set the title of the modal.
          title: dokan.i18n_choose_featured_img,
          button: {
              text: dokan.i18n_choose_featured_img_btn_text,
              close: false
          },
          states: [
              new wp.media.controller.Library({
                  title:     dokan.chooseImage,
                  library:   wp.media.query({ type: 'image' }),
                  multiple:  false,
                  date:      false,
                  priority:  20,
                  suggestedWidth: 1200,
                  suggestedHeight: 795
              }),
              new wp.media.controller.Cropper({                  
                suggestedWidth: 1200,
                  imgSelectOptions: settings.calculateImageSelectOptions,                  
              })
          ]
      });          
      Dokan_Editor.frame.on('select',Dokan_Editor.onSelect);
      Dokan_Editor.frame.on('cropped', Dokan_Editor.onCropped);
      Dokan_Editor.frame.on('skippedcrop', Dokan_Editor.onSkippedCrop);
    },
  	onSelect:function(){  		
  		Dokan_Editor.frame.setState('cropper');
  	},
  	onCropped:function(croppedImage){
  		var url = croppedImage.url,
            attachmentId = croppedImage.attachment_id,
            w = croppedImage.width,
            h = croppedImage.height;
        Dokan_Editor.processImage(url, attachmentId, w, h);
  	},
  	onSkippedCrop:function(selection){
  		var url = selection.get('url'),
            w = selection.get('width'),
            h = selection.get('height');
        Dokan_Editor.processImage(url, selection.id, w, h);
  	},
    processImage:function(url, attachmentId, width, height){
      Dokan_Editor.doProcessImage(url, attachmentId, width, height);
    },
  	setImage: function(url, attachmentId, width, height) {  	
  		var self = Dokan_Editor.self;	
        self.siblings('input.dokan-feat-image-id').val(attachmentId);            
        var instruction = self.closest('.instruction-inside');
        var wrap = instruction.siblings('.image-wrap');            
        wrap.find('img').attr('src', url);
        wrap.find('img').removeAttr( 'srcset' );
        instruction.addClass('dokan-hide');
        wrap.removeClass('dokan-hide');
    },
    setImageGallery:function(url, attachmentId, width, height){
      var self = Dokan_Editor.self;  
      var p_images = self.closest('.dokan-product-gallery').find('#product_images_container ul.product_images'),
          images_gid = self.closest('.dokan-product-gallery').find('#product_image_gallery');
      attachment_ids = [];
      $('<li class="image" data-attachment_id="' + attachmentId + '">\
              <img src="' + url + '" />\
              <a href="#" class="action-delete">&times;</a>\
          </li>').insertBefore( p_images.find('li.add-image') );
      $('#product_images_container ul li.image').css('cursor','default').each(function() {
          var attachment_id = jQuery(this).attr( 'data-attachment_id' );
          attachment_ids.push( attachment_id );
      });
      images_gid.val( attachment_ids.join(',') );
    },
  	calculateImageSelectOptions: function(attachment, controller) {
  		var api = wp.customize;
        var xInit = 1200,
            yInit = 795,
            flexWidth = true,
            flexHeight = true,
            ratio, xImg, yImg, realHeight, realWidth,
            imgSelectOptions;

        realWidth = attachment.get('width');
        realHeight = attachment.get('height');

        this.headerImage = new api.HeaderTool.ImageModel();
        this.headerImage.set({
            themeWidth: xInit,
            themeHeight: yInit,
            themeFlexWidth: flexWidth,
            themeFlexHeight: flexHeight,
            imageWidth: realWidth,
            imageHeight: realHeight
        });

        controller.set('canSkipCrop',!this.headerImage.shouldBeCropped());
        ratio = xInit / yInit;
        xImg = realWidth;
        yImg = realHeight;
        if ( xImg / yImg > ratio ) {
            yInit = yImg;
            xInit = yInit * ratio;
        } else {
            xInit = xImg;
            yInit = xInit / ratio;
        }
        imgSelectOptions = {
            handles: true,
            keys: true,
            instance: true,
            persistent: true,
            imageWidth: realWidth,
            imageHeight: realHeight,
            x1: 0,
            y1: 0,
            x2: xInit,
            y2: yInit
        };
        if (flexHeight === false && flexWidth === false) {
            imgSelectOptions.aspectRatio = xInit + ':' + yInit;
        }
        if (flexHeight === false ) {
            imgSelectOptions.maxHeight = yInit;
        }
        if (flexWidth === false ) {
            imgSelectOptions.maxWidth = xInit;
        }

        return imgSelectOptions;
    }
  };
  Dokan_Editor.init();
})(jQuery);