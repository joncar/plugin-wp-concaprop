<?php

add_action('dokan_product_updated',function($post){
	$product_info = array(
        'ID'             => $post,
        'post_status'    => 'publish'
    );
	wp_update_post( $product_info );	
});

add_action( 'dokan_new_seller_created', function($params){
	if(!empty($_POST['role']) && $_POST['role']=='seller'){
		$user_id = $params;
		$params = get_user_meta($params,'dokan_profile_settings',TRUE);
		$params['location'] = '41.58139,1.62083';
		$params['find_address'] = 'Igualada, provincia de Barcelona, España';
		$params['address'] = [
			'street_1'=>$_POST['dokan_address']['street_1'],
			'street_2'=>$_POST['dokan_address']['street_2'],
			'city'=>$_POST['dokan_address']['city'],
			'zip'=>$_POST['dokan_address']['zip'],
			'country'=>$_POST['dokan_address']['country'],
			'state'=>$_POST['dokan_address']['state']
		];

		update_user_meta( $user_id, 'dokan_profile_settings', $params);
		update_user_meta($user_id,'dokan_geo_latitude','41.58139');
		update_user_meta($user_id,'dokan_geo_longitude','1.62083');
		update_user_meta($user_id,'dokan_geo_public',1);
		update_user_meta($user_id,'dokan_geo_address',$_POST['dokan_address']['city'].','.$_POST['dokan_address']['state'].','.$_POST['dokan_address']['country']);
	}
});

add_action('dokan_seller_registration_field_after',function(){
	require_once JONCARPATH.'/views/register_address.php';
});