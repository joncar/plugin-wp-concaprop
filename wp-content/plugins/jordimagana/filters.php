<?php
/********************************* SETUP WIZARD *************************************/
//Validation
add_filter('woocommerce_registration_errors',function($params){	
	
	if(!empty($_POST['role']) && $_POST['role']=='seller'){
		$requireds = [
			'street_1',
			'city',
			'zip',
			'country',
			'State'
		];
		$labels = [		
			'street_1'=>esc_html_e( 'Street ', 'dokan-lite' ),
			'city'=>esc_html_e( 'City', 'dokan-lite' ),
			'zip'=>esc_html_e( 'Post/ZIP Code', 'dokan-lite' ),
			'country'=>esc_html_e( 'Country ', 'dokan-lite' ),
			'State'=>esc_html_e( 'State ', 'dokan-lite' )
		];
		if(empty($_POST['dokan_address'])){
			$params->add(400,'Faltan los datos del comercio');
			return $params;
		}
		foreach($_POST['dokan_address'] as $n=>$p){
			if(in_array($n,$requireds) && empty($p)){
				$params->add(400,'Falta completar el campo '.$labels[$n]);    
			}
		}
	}

	return $params;	
});