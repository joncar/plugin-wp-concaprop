<?php
/*
Plugin Name: JordiMagana
Plugin URI: https://jordimagana.com
Description: Custom plugin to this webpage
Version: 1
Author: Jonathan Cardozo
Author URI: https://zerobscrm.com
Text Domain: jordimagana
*/
define('JONCARPATH',dirname(__FILE__));

//Class


//Helpers
require_once JONCARPATH.'/actions.php';
require_once JONCARPATH.'/filters.php';
require_once JONCARPATH.'/helpers.php';

function front_scripts($hook){	
	wp_add_inline_script('uri', 'var URI = \''.site_url().'\';' );
    wp_register_script('general', site_url().'/wp-content/plugins/jordimagana/views/js/general.js', array('jquery'), '2.3', true,999);
    wp_enqueue_script('general');

    wp_register_script('dokan-joncar', site_url().'/wp-content/plugins/jordimagana/views/js/dokan-joncar.js', array('jquery'), '2.3', true,999);
    wp_enqueue_script('dokan-joncar');

    wp_register_style( 'frontCSS', site_url().'/wp-content/plugins/jordimagana/views/css/style.css' );
    wp_enqueue_style( 'frontCSS' );     
    
}
add_action("wp_enqueue_scripts", "front_scripts",99);
